package com.sobergem.bookstore.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.BindingAdapter;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.imageview.ShapeableImageView;
import com.sobergem.bookstore.R;
import com.sobergem.bookstore.api.allbooks.Result;
import com.sobergem.bookstore.application.BookStoreApplication;
import com.sobergem.bookstore.databinding.ListFragmentBinding;
import com.sobergem.bookstore.util.BooksDataAdapter;
import com.sobergem.bookstore.viewmodel.BooksViewModel;
import com.squareup.picasso.Picasso;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class BooksListFragment extends Fragment {
    private BooksViewModel booksViewModel;
    private ListFragmentBinding binding;

    private BooksDataAdapter booksDataAdapter;
    @Nullable
    @org.jetbrains.annotations.Nullable
    @Override
    public View onCreateView(@NonNull @NotNull LayoutInflater inflater, @Nullable @org.jetbrains.annotations.Nullable ViewGroup container, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.list_fragment, container, false);
        RecyclerView recyclerView = binding.booksList;
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(requireContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(), DividerItemDecoration.VERTICAL);
        recyclerView.addItemDecoration(dividerItemDecoration);
        booksDataAdapter = new BooksDataAdapter();
        recyclerView.setAdapter(booksDataAdapter);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull @NotNull View view, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        booksViewModel = new ViewModelProvider(requireActivity()).get(BooksViewModel.class);
        booksViewModel.getAllBooks().observe(requireActivity(), new Observer<List<Result>>() {
            @Override
            public void onChanged(List<Result> results) {
                booksDataAdapter.setBookList(results);
            }
        });

    }

    @BindingAdapter("book_img_url")
    public static void imageLoader(ShapeableImageView imageView, String url){
        if(url != null){
            Picasso.with(imageView.getContext()).load(url).into(imageView);
        }
    }

}
