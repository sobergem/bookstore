package com.sobergem.bookstore.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.sobergem.bookstore.R;
import com.sobergem.bookstore.databinding.AddBooksFragmentBinding;
import com.sobergem.bookstore.model.Product;
import com.sobergem.bookstore.viewmodel.BooksViewModel;

import org.jetbrains.annotations.NotNull;

public class AddBooksFragment extends Fragment{
    private AddBooksFragmentBinding binding = null;
    private BooksViewModel booksViewModel;
    private Product product = null;
    @Nullable
    @org.jetbrains.annotations.Nullable
    @Override
    public View onCreateView(@NonNull @NotNull LayoutInflater inflater, @Nullable @org.jetbrains.annotations.Nullable ViewGroup container, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        binding = AddBooksFragmentBinding.inflate(inflater, container, false);
        View view = binding.getRoot();
        return view;
    }

    @Override
    public void onViewCreated(@NonNull @NotNull View view, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        booksViewModel = new ViewModelProvider(requireActivity()).get(BooksViewModel.class);
        setEvents();
    }

    private void setEvents() {
        binding.saveButton.setOnClickListener(v -> {
            String errorMessage = validate();
            if(errorMessage.length()==0){
                booksViewModel.insert(product);
                Toast.makeText(requireActivity(), "Product Added Successfully", Toast.LENGTH_SHORT).show();
            }else{
                Toast.makeText(requireActivity(), errorMessage, Toast.LENGTH_SHORT).show();
            }

        });
    }

    private String validate() {
        String errorMessage = "";
        product = new Product();
        if(binding.productName.getText() != null && binding.productName.getText().toString().trim().length()>0){
            product.setProduct_name(binding.productName.getText().toString());
        }else{
            product = null;
            errorMessage = getString(R.string.please_enter_product_name);
            return errorMessage;
        }

        if(binding.productDescription.getText() != null && binding.productDescription.getText().toString().trim().length()>0){
            product.setProduct_desc(binding.productDescription.getText().toString());
        }else{
            product = null;
            errorMessage = getString(R.string.please_enter_product_description);
            return errorMessage;
        }

        if(binding.quantity.getText() != null && binding.quantity.getText().toString().trim().length() >0)
        {
            product.setProduct_quantity(binding.quantity.getText().toString());
        }else{
            product = null;
            errorMessage = getString(R.string.please_add_quantity);
            return errorMessage;
        }

        if(binding.price.getText() != null && binding.price.getText().toString().trim().length()>0){
            product.setProduct_price(binding.price.getText().toString());
        }else{
            product = null;
            errorMessage = getString(R.string.please_enter_price);
            return errorMessage;
        }
        return errorMessage;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}
