package com.sobergem.bookstore.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;

import com.sobergem.bookstore.databinding.SelectionFragmentBinding;
import com.sobergem.bookstore.viewmodel.BooksViewModel;

import org.jetbrains.annotations.NotNull;


public class SelectionFragment extends Fragment {
    private BooksViewModel booksViewModel;
    private SelectionFragmentBinding binding = null;

    @Nullable
    @org.jetbrains.annotations.Nullable
    @Override
    public View onCreateView(@NonNull @NotNull LayoutInflater inflater, @Nullable @org.jetbrains.annotations.Nullable ViewGroup container, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        binding = SelectionFragmentBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    private void setEvents() {
        binding.addProductButton.setOnClickListener(v -> addBooks());

        binding.booksButton.setOnClickListener(v -> showBooks());

        binding.syncProducts.setOnClickListener(v -> {
            syncNewProducts();
        });
    }

    private void syncNewProducts() {
        booksViewModel.synNewProducts().observe(requireActivity(), rootDictionary -> {
            if(rootDictionary!= null) {
                switch (rootDictionary.status) {
                    case SUCCESS:
                        Toast.makeText(requireActivity(), "Sync Completed", Toast.LENGTH_SHORT).show();
                        break;
                    case LOADING:
                        Toast.makeText(requireActivity(), "Sync InProgress", Toast.LENGTH_SHORT).show();
                        break;
                    case ERROR:
                        Toast.makeText(requireActivity(), rootDictionary.message, Toast.LENGTH_LONG).show();
                        break;
                }
            }
        });
    }

    public void addBooks(){
        NavDirections action = SelectionFragmentDirections.selectionToAddBooks();
        Navigation.findNavController(binding.addProductButton).navigate(action);
    }

    public void showBooks(){
        NavDirections action = SelectionFragmentDirections.selectionToList();
        Navigation.findNavController(binding.booksButton).navigate(action);
    }

    @Override
    public void onViewCreated(@NonNull @NotNull View view, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        booksViewModel = new ViewModelProvider(requireActivity()).get(BooksViewModel.class);
        booksViewModel.getNewProducts().observe(requireActivity(), products -> {
            if(products != null && products.size()>0){
                binding.syncProducts.setVisibility(View.VISIBLE);
            }else{
                binding.syncProducts.setVisibility(View.GONE);
            }
        });
        setEvents();
        booksViewModel.subscribeToast().observe(requireActivity(), message ->{
            if(message != null){
                Toast.makeText(requireActivity(), message, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
        booksViewModel.getNewProducts().removeObservers(requireActivity());
        booksViewModel.subscribeToast().removeObservers(requireActivity());
    }
}
