package com.sobergem.bookstore.database;

import android.app.Application;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.sobergem.bookstore.api.allbooks.Dictionary;
import com.sobergem.bookstore.api.allbooks.Result;
import com.sobergem.bookstore.api.models.Root;
import com.sobergem.bookstore.api.retrofit.RetrofitRequest;
import com.sobergem.bookstore.dao.ProductDao;
import com.sobergem.bookstore.model.Product;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BookRepository {
    private final ProductDao productDao;
    private final LiveData<List<Product>> allProducts;
    private List<Product> products;

    private  MutableLiveData<String> message = new MutableLiveData<>();

    private static final String url = "http://15.206.209.151/";

    public BookRepository(Application application) {
        BookStoreDatabase db = BookStoreDatabase.getDatabaseInstance(application);
        productDao = db.productDao();
        allProducts = productDao.getProducts();
    }

    public LiveData<List<Product>> getAllProducts() {
        return allProducts;
    }

    public void insert(Product product) {
        BookStoreDatabase.databaseWriteExecutor.execute(() -> productDao.insertProducts(product));
    }

    public void delete(Product product) {
        BookStoreDatabase.databaseWriteExecutor.execute(() -> productDao.deleteProducts(product));
    }

    public LiveData<Dictionary<Root>> syncNewProducts() {
        products =  allProducts.getValue();
        if(products!= null && products.size()>0) {
            return sync(products.get(0));
        }else{
            return null;
        }
    }

    private LiveData<Dictionary<Root>> sync(Product product){
        final MutableLiveData<Dictionary<Root>> syncResult = new MutableLiveData<>();
        RetrofitRequest retrofitRequest = new RetrofitRequest();
        Call<Root> call = retrofitRequest.addBook(url,product);
        call.enqueue(new Callback<Root>() {
            @Override
            public void onResponse(Call<Root> call, Response<Root> response) {
                if(response.isSuccessful() && response.code()==200){
                    products.remove(product);
                    delete(product);
                    if(products.size()>0) {
                        sync(products.get(0));
                        if(response.body() != null) {
                            Root body = response.body();
                            syncResult.setValue(Dictionary.loading(body));
                        }

                    }
                    else{
                        if(response.body() != null){
                            Root body = response.body();
                            syncResult.setValue(Dictionary.success(body));
                        }
                    }
                }else{
                    if(response.message() != null) {
                        syncResult.setValue(Dictionary.error(response.message(), null));
                    }
                    else{
                       syncResult.setValue(Dictionary.error("Error Occurred", null));
                    }
                }
            }

            @Override
            public void onFailure(Call<Root> call, Throwable t) {
                if(t != null && t.getMessage() != null){
                    syncResult.setValue(Dictionary.error(t.getMessage(), null));
                }else{
                    syncResult.setValue(Dictionary.error("Error Occurred", null));
                }
            }
        });
        return syncResult;
    }

    public LiveData<List<Result>> getAllBooks(){
        MutableLiveData<List<Result>> results = new MutableLiveData<>();
        RetrofitRequest retrofitRequest = new RetrofitRequest();
        Call<com.sobergem.bookstore.api.allbooks.Root> getAllBooksCall = retrofitRequest.getAllBooks(url);
        getAllBooksCall.enqueue(new Callback<com.sobergem.bookstore.api.allbooks.Root>() {
            @Override
            public void onResponse(Call<com.sobergem.bookstore.api.allbooks.Root> call, Response<com.sobergem.bookstore.api.allbooks.Root> response) {
                if(response.isSuccessful() && response.code() == 200){
                    results.setValue(response.body().results);
                }else{
                    if(response.message() != null){
                        message.setValue(response.message());
                    }else{
                        message.setValue("Error Occurred");
                    }
                }
            }

            @Override
            public void onFailure(Call<com.sobergem.bookstore.api.allbooks.Root> call, Throwable t) {
                if(t != null && t.getMessage() != null){
                    message.setValue(t.getMessage());
                }else{
                    message.setValue("Error Occurred");
                }
            }
        });
        return results;
    }

    public LiveData<String> getMessage(){
        return message;
    }
}
