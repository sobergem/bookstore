package com.sobergem.bookstore.database;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.sobergem.bookstore.dao.ProductDao;
import com.sobergem.bookstore.model.Product;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Database(entities = {Product.class}, version =1,exportSchema = false)
public abstract class BookStoreDatabase extends RoomDatabase {
    public abstract ProductDao productDao();

    private static volatile BookStoreDatabase bookStoreDatabase;

    private static final String DATABASE_NAME = "BookDatabase";

    private static final int NUMBER_OF_THREADS = 4;
    static final ExecutorService databaseWriteExecutor = Executors.newFixedThreadPool(NUMBER_OF_THREADS);

    static BookStoreDatabase getDatabaseInstance(final Context context){
        if(bookStoreDatabase == null){
            bookStoreDatabase = Room.databaseBuilder(context, BookStoreDatabase.class, DATABASE_NAME).build();
        }
        return bookStoreDatabase;
    }
}
