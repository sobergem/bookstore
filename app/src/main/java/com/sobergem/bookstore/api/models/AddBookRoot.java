package com.sobergem.bookstore.api.models;

public class AddBookRoot
{
    private AddBookResults addBookResults;

    public void setResults(AddBookResults addBookResults){
        this.addBookResults = addBookResults;
    }
    public AddBookResults getResults(){
        return this.addBookResults;
    }

    @Override
    public String toString() {
        return "AddBookRoot{" +
                "addBookResults=" + addBookResults +
                '}';
    }
}

