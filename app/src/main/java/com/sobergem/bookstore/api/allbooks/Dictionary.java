package com.sobergem.bookstore.api.allbooks;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class Dictionary<T> {
    @NonNull public final Status status;
    @NonNull public final T data;
    @NonNull public final String message;

    public Dictionary(@NonNull Status status, @NonNull T data, @NonNull String message) {
        this.status = status;
        this.data = data;
        this.message = message;
    }

    public static <T> Dictionary<T> success(@NonNull T data) {
        return new Dictionary<>(Status.SUCCESS, data, null);
    }

    public static <T> Dictionary<T> error(String msg, @Nullable T data) {
        return new Dictionary<>(Status.ERROR, data, msg);
    }

    public static <T> Dictionary<T> loading(@Nullable T data) {
        return new Dictionary<>(Status.LOADING, data, null);
    }


    public enum Status{SUCCESS, ERROR, LOADING}
}
