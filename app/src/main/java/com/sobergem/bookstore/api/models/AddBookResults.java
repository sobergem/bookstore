package com.sobergem.bookstore.api.models;

public class AddBookResults
{
    private int success;

    private String message;

    public void setSuccess(int success){
        this.success = success;
    }
    public int getSuccess(){
        return this.success;
    }
    public void setMessage(String message){
        this.message = message;
    }
    public String getMessage(){
        return this.message;
    }

    @Override
    public String toString() {
        return "AddBookResults{" +
                "success=" + success +
                ", message='" + message + '\'' +
                '}';
    }
}