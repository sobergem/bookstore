package com.sobergem.bookstore.api.models;

public class AllBooksResults
{
    private int book_id;

    private String book_name;

    private String book_desc;

    private String book_author;

    private String book_price;

    private String book_img_url;

    public void setBook_id(int book_id){
        this.book_id = book_id;
    }
    public int getBook_id(){
        return this.book_id;
    }
    public void setBook_name(String book_name){
        this.book_name = book_name;
    }
    public String getBook_name(){
        return this.book_name;
    }
    public void setBook_desc(String book_desc){
        this.book_desc = book_desc;
    }
    public String getBook_desc(){
        return this.book_desc;
    }
    public void setBook_author(String book_author){
        this.book_author = book_author;
    }
    public String getBook_author(){
        return this.book_author;
    }
    public void setBook_price(String book_price){
        this.book_price = book_price;
    }
    public String getBook_price(){
        return this.book_price;
    }
    public void setBook_img_url(String book_img_url){
        this.book_img_url = book_img_url;
    }
    public String getBook_img_url(){
        return this.book_img_url;
    }
}
