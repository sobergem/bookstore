package com.sobergem.bookstore.api.retrofit;

import com.sobergem.bookstore.api.models.AddBookRoot;
import com.sobergem.bookstore.api.models.AllBooksResults;
import com.sobergem.bookstore.api.models.Root;
import com.sobergem.bookstore.model.Product;

import retrofit2.Call;

public class RetrofitRequest {
    public Call<com.sobergem.bookstore.api.allbooks.Root> getAllBooks(String url){
        return RetrofitApi.getWebservice(url).getBooks();
    }

    public Call<Root> addBook(String url, Product body){
        return RetrofitApi.getWebservice(url).addBooks(body);
    }
}
