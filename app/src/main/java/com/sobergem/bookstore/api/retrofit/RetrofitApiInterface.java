package com.sobergem.bookstore.api.retrofit;

import com.sobergem.bookstore.api.models.AllBooksResults;
import com.sobergem.bookstore.api.models.Root;
import com.sobergem.bookstore.model.Product;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface RetrofitApiInterface {
    @GET("api/getAllAvailableBooks")
    public Call<com.sobergem.bookstore.api.allbooks.Root> getBooks();

    @POST("api/addNewProduct")
    @Headers({"Content-Type:application/json", "Accept:*/*"})
    public Call<Root> addBooks(@Body Product body);
}
