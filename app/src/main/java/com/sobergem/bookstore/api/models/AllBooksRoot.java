package com.sobergem.bookstore.api.models;

import java.util.List;

public class AllBooksRoot
{
    private List<AllBooksResults> results;

    public void setResults(List<AllBooksResults> results){
        this.results = results;
    }
    public List<AllBooksResults> getResults(){
        return this.results;
    }
}

