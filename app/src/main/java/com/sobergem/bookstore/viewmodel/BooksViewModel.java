package com.sobergem.bookstore.viewmodel;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.sobergem.bookstore.api.allbooks.Dictionary;
import com.sobergem.bookstore.api.allbooks.Result;
import com.sobergem.bookstore.api.models.Root;
import com.sobergem.bookstore.database.BookRepository;
import com.sobergem.bookstore.model.Product;

import java.util.List;

public class BooksViewModel extends AndroidViewModel {
    private final BookRepository bookRepository;

    private final LiveData<List<Product>> allProducts;
    private final LiveData<List<Result>> allBooks;

    private final MutableLiveData<String> toastMessage = new MutableLiveData<>();


    public BooksViewModel(Application application) {
        super(application);
        bookRepository = new BookRepository(application);
        allProducts = bookRepository.getAllProducts();
        allBooks = bookRepository.getAllBooks();
    }

    public LiveData<List<Product>> getNewProducts() {
        return allProducts;
    }

    public void insert(Product product) {
        bookRepository.insert(product);
    }

    public LiveData<Dictionary<Root>> synNewProducts() {
        return bookRepository.syncNewProducts();
    }

    public LiveData<List<Result>> getAllBooks(){
        if(allBooks == null || allBooks.getValue()== null){
            setToast("No Details Found");
        }
        return allBooks;
    }

    public void setToast(String message){
        toastMessage.setValue(message);
    }

    public LiveData<String> subscribeToast(){
        return toastMessage;
    }
}
