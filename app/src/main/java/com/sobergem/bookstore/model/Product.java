package com.sobergem.bookstore.model;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

@Entity
public class Product {

    @PrimaryKey(autoGenerate = true)
    public int productID;

    @NonNull
    public String product_name;
    public String product_desc;
    public String product_quantity;
    public String product_price;
    @Ignore
    public String user_mobile_no;

    public Product(String product_name, String product_desc, String product_quantity, String product_price) {
        this.product_name = product_name;
        this.product_desc = product_desc;
        this.product_quantity = product_quantity;
        this.product_price = product_price;
        this.user_mobile_no = "8618403837";
    }

    @Ignore
    public Product() {
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public String getProduct_desc() {
        return product_desc;
    }

    public void setProduct_desc(String product_desc) {
        this.product_desc = product_desc;
    }

    public String getProduct_quantity() {
        return product_quantity;
    }

    public void setProduct_quantity(String product_quantity) {
        this.product_quantity = product_quantity;
    }

    public String getProduct_price() {
        return product_price;
    }

    public void setProduct_price(String product_price) {
        this.product_price = product_price;
    }

    public String getUser_mobile_no() {
        return user_mobile_no;
    }
}
