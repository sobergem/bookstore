package com.sobergem.bookstore.activity;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.TaskStackBuilder;


import com.sobergem.bookstore.R;

public class MainActivity extends BaseActivity{
    @Override
    public void onCreateSupportNavigateUpTaskStack(@NonNull @org.jetbrains.annotations.NotNull TaskStackBuilder builder) {
        super.onCreateSupportNavigateUpTaskStack(builder);
    }

    @Override
    protected void onCreate(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);
    }
}
