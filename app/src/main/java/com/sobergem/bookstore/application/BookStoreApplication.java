package com.sobergem.bookstore.application;

import android.app.Application;
import android.content.Context;

import com.sobergem.bookstore.util.FontsOverride;

public class BookStoreApplication extends Application {
    public static Context applicationContext;
    @Override
    public void onCreate() {
        super.onCreate();
        applicationContext = getBaseContext();
        String regularFont = "fonts/quicksand_regular.ttf";
        FontsOverride.setDefaultFont(this,"DEFAULT", regularFont);
        FontsOverride.setDefaultFont(this, "MONOSPACE", regularFont);
        FontsOverride.setDefaultFont(this, "SERIF", regularFont);
        FontsOverride.setDefaultFont(this, "SANS_SERIF",regularFont);

    }
}
