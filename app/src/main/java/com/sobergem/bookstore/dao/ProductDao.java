package com.sobergem.bookstore.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import com.sobergem.bookstore.model.Product;

import java.util.List;

@Dao
public interface ProductDao {

    @Query("Select * from Product")
    LiveData<List<Product>> getProducts();

    @Insert
    void insertProducts(Product...products);

    @Delete
    void deleteProducts(Product product);
}
