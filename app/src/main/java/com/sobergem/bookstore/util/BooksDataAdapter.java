package com.sobergem.bookstore.util;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.sobergem.bookstore.R;
import com.sobergem.bookstore.api.allbooks.Result;
import com.sobergem.bookstore.databinding.BooksItemBinding;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class BooksDataAdapter extends RecyclerView.Adapter<BooksDataAdapter.BooksViewHolder> {

    private List<Result> allBooks;

    @NonNull
    @NotNull
    @Override
    public BooksViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        BooksItemBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.books_item, parent, false);
        return new BooksViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull BooksDataAdapter.BooksViewHolder holder, int position) {
        Result result = allBooks.get(position);
        holder.binding.setResult(result);
    }

    @Override
    public int getItemCount() {
        if(allBooks != null){
            return allBooks.size();
        }else{
            return 0;
        }
    }

    public void setBookList(List<Result> results){
        this.allBooks = results;
        notifyDataSetChanged();
    }


    class BooksViewHolder extends RecyclerView.ViewHolder{
        private BooksItemBinding binding;
        public BooksViewHolder(BooksItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
